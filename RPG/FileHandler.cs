﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CharacterLibrary;
using Newtonsoft.Json;

namespace RPG
{
    static class FileHandler
    {
        static readonly string characterFolderPath = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}/RPG";

        public static bool SaveCharacter(Character character)
        {
            try
            {
                if (!Directory.Exists(characterFolderPath)) Directory.CreateDirectory(characterFolderPath);
                File.WriteAllText($"{characterFolderPath}/{character.Name}.json", character.ToJson());
            }
            catch(NullReferenceException)
            {
                return false;
            }

            return true;
        }

        public static Character LoadCharacter(string name)
        {
            string characterFile = $"{characterFolderPath}/{name}.json";
            if(File.Exists(characterFile) && new FileInfo(characterFile).Length > 0)
            {
                return FromJson(File.ReadAllText(characterFile));
            }
            else
            {
                return null;
            }
        }

        public static IEnumerable<Character> LoadAllCharacters()
        {
            foreach (string file in Directory.GetFiles(characterFolderPath)) yield return FromJson(File.ReadAllText(file));
        }



        static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All
        };

        public static string ToJson(this Character c) => JsonConvert.SerializeObject(c, JsonSettings);

        public static Character FromJson(string json) => JsonConvert.DeserializeObject<Character>(json, JsonSettings);
    }
}
