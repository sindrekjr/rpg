﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CharacterLibrary;
//using CharacterLibrary.Alignment;

namespace RPG
{
    public partial class MainForm : Form
    {
        readonly List<Character> Characters;

        Character _c;
        Character SelectedCharacter
        {
            get => _c;
            set {
                _c = value;
                charName.DataBindings.Clear();
                charName.DataBindings.Add("Text", _c, "Name");

                statsValHP.DataBindings.Clear();
                statsValHP.DataBindings.Add("Text", _c, "HP");

                statsValStr.DataBindings.Clear();
                statsValStr.DataBindings.Add("Text", _c, "Strength");

                statsValAgl.DataBindings.Clear();
                statsValAgl.DataBindings.Add("Text", _c, "Agility");

                statsValInt.DataBindings.Clear();
                statsValInt.DataBindings.Add("Text", _c, "Intelligence");

                statsValStamina.DataBindings.Clear();
                statsValStamina.DataBindings.Add("Text", _c, "Stamina");

                statsValAR.DataBindings.Clear();
                statsValAR.DataBindings.Add("Text", _c, "ArmorRating");

                statsValTHAR0.DataBindings.Clear();
                statsValTHAR0.DataBindings.Add("Text", _c, "THAR0");

                classSelectBox.SelectedIndex = classSelectBox.FindStringExact(value.GetType().Name);
                descBox.Text = FileHandler.ToJson(value);
            }
        }

        public MainForm() => InitializeComponent();

        public MainForm(List<Character> characters) : this() => Characters = characters;

        public MainForm(List<Character> characters, List<Type> allClassTypes) : this(characters)
        {
            classSelectBox.DataSource = new BindingSource { DataSource = allClassTypes };
            classSelectBox.DisplayMember = "Name";

            /*alignSelectBox.DataSource = new BindingSource
            {
                DataSource = ((Alignment[]) Enum.GetValues(typeof(Alignment))).Select(a => StrVal.GetStringValue(a))
            };*/

            listCharacters.DataSource = new BindingSource { DataSource = Characters };
            listCharacters.DisplayMember = "Name";
        }

        void BtnRndChar_Click(object sender, EventArgs e) => SelectedCharacter = Characters[new Random().Next(Characters.Count)];

        void BtnLoadChar_Click(object sender, EventArgs e) => SelectedCharacter = (Character) listCharacters.SelectedItem;

        void BtnSaveChar_Click(object sender, EventArgs e)
        {
            if(SelectedCharacter == null)
            {
                try
                {
                    SelectedCharacter = (Character)Activator.CreateInstance(typeof(Character).Assembly.GetType(classSelectBox.SelectedItem.ToString()), charName.Text);
                }
                catch(MissingMethodException)
                {
                    SelectedCharacter = (Character)Activator.CreateInstance(typeof(Character).Assembly.GetType(classSelectBox.SelectedItem.ToString()));
                }
            }

            //FileHandler.SaveCharacter(SelectedCharacter);
            SQL.SaveCharacter(SelectedCharacter);
        }

        private C CreateCharacter<C>() where C : Character
        {
            return (C) Activator.CreateInstance(typeof(C), null, null);
        }
    }
}
