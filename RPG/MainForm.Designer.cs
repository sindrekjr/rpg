﻿namespace RPG
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnRndChar = new System.Windows.Forms.Button();
            this.charStats = new System.Windows.Forms.GroupBox();
            this.baseStatsTable = new System.Windows.Forms.TableLayoutPanel();
            this.statsLabelInt = new System.Windows.Forms.Label();
            this.statsLabelAgl = new System.Windows.Forms.Label();
            this.statsLabelStr = new System.Windows.Forms.Label();
            this.statsLabelHP = new System.Windows.Forms.Label();
            this.statsLabelStamina = new System.Windows.Forms.Label();
            this.statsLabelAR = new System.Windows.Forms.Label();
            this.statsLabelTHAR0 = new System.Windows.Forms.Label();
            this.statsValHP = new System.Windows.Forms.TextBox();
            this.statsValStr = new System.Windows.Forms.TextBox();
            this.statsValAgl = new System.Windows.Forms.TextBox();
            this.statsValInt = new System.Windows.Forms.TextBox();
            this.statsValStamina = new System.Windows.Forms.TextBox();
            this.statsValAR = new System.Windows.Forms.TextBox();
            this.statsValTHAR0 = new System.Windows.Forms.TextBox();
            this.btnSaveChar = new System.Windows.Forms.Button();
            this.charName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.alignSelectBox = new System.Windows.Forms.ComboBox();
            this.classSelectBox = new System.Windows.Forms.ComboBox();
            this.descBox = new System.Windows.Forms.RichTextBox();
            this.btnLoadChar = new System.Windows.Forms.Button();
            this.listCharacters = new System.Windows.Forms.ListBox();
            this.charStats.SuspendLayout();
            this.baseStatsTable.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRndChar
            // 
            this.btnRndChar.Location = new System.Drawing.Point(330, 16);
            this.btnRndChar.Margin = new System.Windows.Forms.Padding(4);
            this.btnRndChar.Name = "btnRndChar";
            this.btnRndChar.Size = new System.Drawing.Size(187, 31);
            this.btnRndChar.TabIndex = 0;
            this.btnRndChar.Text = "Get Random Character";
            this.btnRndChar.UseVisualStyleBackColor = true;
            this.btnRndChar.Click += new System.EventHandler(this.BtnRndChar_Click);
            // 
            // charStats
            // 
            this.charStats.BackColor = System.Drawing.Color.Transparent;
            this.charStats.Controls.Add(this.baseStatsTable);
            this.charStats.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.charStats.Location = new System.Drawing.Point(330, 111);
            this.charStats.Margin = new System.Windows.Forms.Padding(4);
            this.charStats.Name = "charStats";
            this.charStats.Padding = new System.Windows.Forms.Padding(4);
            this.charStats.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.charStats.Size = new System.Drawing.Size(211, 376);
            this.charStats.TabIndex = 2;
            this.charStats.TabStop = false;
            this.charStats.Text = "Base Stats";
            // 
            // baseStatsTable
            // 
            this.baseStatsTable.ColumnCount = 3;
            this.baseStatsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 93.12977F));
            this.baseStatsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.870229F));
            this.baseStatsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.baseStatsTable.Controls.Add(this.statsLabelInt, 0, 3);
            this.baseStatsTable.Controls.Add(this.statsLabelAgl, 0, 2);
            this.baseStatsTable.Controls.Add(this.statsLabelHP, 0, 0);
            this.baseStatsTable.Controls.Add(this.statsLabelStamina, 0, 4);
            this.baseStatsTable.Controls.Add(this.statsLabelAR, 0, 5);
            this.baseStatsTable.Controls.Add(this.statsLabelTHAR0, 0, 6);
            this.baseStatsTable.Controls.Add(this.statsValHP, 2, 0);
            this.baseStatsTable.Controls.Add(this.statsValStr, 2, 1);
            this.baseStatsTable.Controls.Add(this.statsValAgl, 2, 2);
            this.baseStatsTable.Controls.Add(this.statsValInt, 2, 3);
            this.baseStatsTable.Controls.Add(this.statsValStamina, 2, 4);
            this.baseStatsTable.Controls.Add(this.statsValAR, 2, 5);
            this.baseStatsTable.Controls.Add(this.statsValTHAR0, 2, 6);
            this.baseStatsTable.Controls.Add(this.statsLabelStr, 0, 1);
            this.baseStatsTable.Location = new System.Drawing.Point(4, 23);
            this.baseStatsTable.Name = "baseStatsTable";
            this.baseStatsTable.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.baseStatsTable.RowCount = 8;
            this.baseStatsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.baseStatsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.baseStatsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.baseStatsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.baseStatsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.baseStatsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.baseStatsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.baseStatsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.baseStatsTable.Size = new System.Drawing.Size(200, 346);
            this.baseStatsTable.TabIndex = 3;
            // 
            // statsLabelInt
            // 
            this.statsLabelInt.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.statsLabelInt.AutoSize = true;
            this.statsLabelInt.Location = new System.Drawing.Point(40, 96);
            this.statsLabelInt.Name = "statsLabelInt";
            this.statsLabelInt.Size = new System.Drawing.Size(79, 17);
            this.statsLabelInt.TabIndex = 2;
            this.statsLabelInt.Text = "Intelligence";
            // 
            // statsLabelAgl
            // 
            this.statsLabelAgl.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.statsLabelAgl.AutoSize = true;
            this.statsLabelAgl.Location = new System.Drawing.Point(74, 66);
            this.statsLabelAgl.Name = "statsLabelAgl";
            this.statsLabelAgl.Size = new System.Drawing.Size(45, 17);
            this.statsLabelAgl.TabIndex = 1;
            this.statsLabelAgl.Text = "Agility";
            // 
            // statsLabelStr
            // 
            this.statsLabelStr.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.statsLabelStr.AutoSize = true;
            this.statsLabelStr.Location = new System.Drawing.Point(57, 36);
            this.statsLabelStr.Name = "statsLabelStr";
            this.statsLabelStr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.statsLabelStr.Size = new System.Drawing.Size(62, 17);
            this.statsLabelStr.TabIndex = 0;
            this.statsLabelStr.Text = "Strength";
            // 
            // statsLabelHP
            // 
            this.statsLabelHP.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.statsLabelHP.AutoSize = true;
            this.statsLabelHP.Location = new System.Drawing.Point(92, 6);
            this.statsLabelHP.Name = "statsLabelHP";
            this.statsLabelHP.Size = new System.Drawing.Size(27, 17);
            this.statsLabelHP.TabIndex = 3;
            this.statsLabelHP.Text = "HP";
            // 
            // statsLabelStamina
            // 
            this.statsLabelStamina.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.statsLabelStamina.AutoSize = true;
            this.statsLabelStamina.Location = new System.Drawing.Point(60, 126);
            this.statsLabelStamina.Name = "statsLabelStamina";
            this.statsLabelStamina.Size = new System.Drawing.Size(59, 17);
            this.statsLabelStamina.TabIndex = 12;
            this.statsLabelStamina.Text = "Stamina";
            // 
            // statsLabelAR
            // 
            this.statsLabelAR.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.statsLabelAR.AutoSize = true;
            this.statsLabelAR.Location = new System.Drawing.Point(28, 156);
            this.statsLabelAR.Name = "statsLabelAR";
            this.statsLabelAR.Size = new System.Drawing.Size(91, 17);
            this.statsLabelAR.TabIndex = 13;
            this.statsLabelAR.Text = "Armor Rating";
            // 
            // statsLabelTHAR0
            // 
            this.statsLabelTHAR0.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.statsLabelTHAR0.AutoSize = true;
            this.statsLabelTHAR0.Location = new System.Drawing.Point(65, 186);
            this.statsLabelTHAR0.Name = "statsLabelTHAR0";
            this.statsLabelTHAR0.Size = new System.Drawing.Size(54, 17);
            this.statsLabelTHAR0.TabIndex = 14;
            this.statsLabelTHAR0.Text = "THAR0";
            // 
            // statsValHP
            // 
            this.statsValHP.Location = new System.Drawing.Point(134, 3);
            this.statsValHP.Name = "statsValHP";
            this.statsValHP.Size = new System.Drawing.Size(57, 23);
            this.statsValHP.TabIndex = 11;
            this.statsValHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // statsValStr
            // 
            this.statsValStr.Location = new System.Drawing.Point(134, 33);
            this.statsValStr.Name = "statsValStr";
            this.statsValStr.Size = new System.Drawing.Size(57, 23);
            this.statsValStr.TabIndex = 8;
            this.statsValStr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // statsValAgl
            // 
            this.statsValAgl.Location = new System.Drawing.Point(134, 63);
            this.statsValAgl.Name = "statsValAgl";
            this.statsValAgl.Size = new System.Drawing.Size(57, 23);
            this.statsValAgl.TabIndex = 9;
            this.statsValAgl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // statsValInt
            // 
            this.statsValInt.Location = new System.Drawing.Point(134, 93);
            this.statsValInt.Name = "statsValInt";
            this.statsValInt.Size = new System.Drawing.Size(57, 23);
            this.statsValInt.TabIndex = 10;
            this.statsValInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // statsValStamina
            // 
            this.statsValStamina.Location = new System.Drawing.Point(134, 123);
            this.statsValStamina.Name = "statsValStamina";
            this.statsValStamina.Size = new System.Drawing.Size(57, 23);
            this.statsValStamina.TabIndex = 15;
            this.statsValStamina.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // statsValAR
            // 
            this.statsValAR.Location = new System.Drawing.Point(134, 153);
            this.statsValAR.Name = "statsValAR";
            this.statsValAR.Size = new System.Drawing.Size(57, 23);
            this.statsValAR.TabIndex = 16;
            this.statsValAR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // statsValTHAR0
            // 
            this.statsValTHAR0.Location = new System.Drawing.Point(134, 183);
            this.statsValTHAR0.Name = "statsValTHAR0";
            this.statsValTHAR0.Size = new System.Drawing.Size(57, 23);
            this.statsValTHAR0.TabIndex = 17;
            this.statsValTHAR0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnSaveChar
            // 
            this.btnSaveChar.Location = new System.Drawing.Point(330, 53);
            this.btnSaveChar.Name = "btnSaveChar";
            this.btnSaveChar.Size = new System.Drawing.Size(187, 31);
            this.btnSaveChar.TabIndex = 3;
            this.btnSaveChar.Text = "Save Character";
            this.btnSaveChar.UseVisualStyleBackColor = true;
            this.btnSaveChar.Click += new System.EventHandler(this.BtnSaveChar_Click);
            // 
            // charName
            // 
            this.charName.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.25F);
            this.charName.Location = new System.Drawing.Point(617, 53);
            this.charName.Name = "charName";
            this.charName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.charName.Size = new System.Drawing.Size(277, 37);
            this.charName.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(611, 169);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(293, 109);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.UseCompatibleTextRendering = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.alignSelectBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.classSelectBox, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 13);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(281, 90);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // alignSelectBox
            // 
            this.alignSelectBox.FormattingEnabled = true;
            this.alignSelectBox.Location = new System.Drawing.Point(143, 48);
            this.alignSelectBox.Name = "alignSelectBox";
            this.alignSelectBox.Size = new System.Drawing.Size(134, 25);
            this.alignSelectBox.TabIndex = 6;
            // 
            // classSelectBox
            // 
            this.classSelectBox.FormattingEnabled = true;
            this.classSelectBox.Location = new System.Drawing.Point(3, 48);
            this.classSelectBox.Name = "classSelectBox";
            this.classSelectBox.Size = new System.Drawing.Size(134, 25);
            this.classSelectBox.TabIndex = 0;
            // 
            // descBox
            // 
            this.descBox.Location = new System.Drawing.Point(611, 285);
            this.descBox.Name = "descBox";
            this.descBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.descBox.Size = new System.Drawing.Size(293, 202);
            this.descBox.TabIndex = 6;
            this.descBox.Text = "";
            // 
            // btnLoadChar
            // 
            this.btnLoadChar.Location = new System.Drawing.Point(198, 449);
            this.btnLoadChar.Name = "btnLoadChar";
            this.btnLoadChar.Size = new System.Drawing.Size(125, 31);
            this.btnLoadChar.TabIndex = 7;
            this.btnLoadChar.Text = "Load Character";
            this.btnLoadChar.UseVisualStyleBackColor = true;
            this.btnLoadChar.Click += new System.EventHandler(this.BtnLoadChar_Click);
            // 
            // listCharacters
            // 
            this.listCharacters.FormattingEnabled = true;
            this.listCharacters.ItemHeight = 17;
            this.listCharacters.Location = new System.Drawing.Point(17, 257);
            this.listCharacters.Name = "listCharacters";
            this.listCharacters.Size = new System.Drawing.Size(306, 174);
            this.listCharacters.TabIndex = 8;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(207)))), ((int)(((byte)(153)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(930, 540);
            this.Controls.Add(this.listCharacters);
            this.Controls.Add(this.btnLoadChar);
            this.Controls.Add(this.descBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.charName);
            this.Controls.Add(this.btnSaveChar);
            this.Controls.Add(this.charStats);
            this.Controls.Add(this.btnRndChar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "MainForm";
            this.charStats.ResumeLayout(false);
            this.baseStatsTable.ResumeLayout(false);
            this.baseStatsTable.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRndChar;
        private System.Windows.Forms.GroupBox charStats;
        private System.Windows.Forms.TableLayoutPanel baseStatsTable;
        private System.Windows.Forms.Label statsLabelStr;
        private System.Windows.Forms.Label statsLabelAgl;
        private System.Windows.Forms.Label statsLabelInt;
        private System.Windows.Forms.Label statsLabelHP;
        private System.Windows.Forms.Button btnSaveChar;
        private System.Windows.Forms.TextBox statsValStr;
        private System.Windows.Forms.TextBox charName;
        private System.Windows.Forms.TextBox statsValAgl;
        private System.Windows.Forms.TextBox statsValInt;
        private System.Windows.Forms.TextBox statsValHP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox alignSelectBox;
        private System.Windows.Forms.ComboBox classSelectBox;
        private System.Windows.Forms.RichTextBox descBox;
        private System.Windows.Forms.Label statsLabelStamina;
        private System.Windows.Forms.Label statsLabelAR;
        private System.Windows.Forms.Label statsLabelTHAR0;
        private System.Windows.Forms.TextBox statsValStamina;
        private System.Windows.Forms.TextBox statsValAR;
        private System.Windows.Forms.Button btnLoadChar;
        private System.Windows.Forms.ListBox listCharacters;
        private System.Windows.Forms.TextBox statsValTHAR0;
    }
}

