﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CharacterLibrary;

namespace RPG
{
    static class SQL
    {
        static readonly string CONNECTION_STRING = GetConnectionString();

        static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder
            {
                DataSource = @"localhost\SQLEXPRESS",
                InitialCatalog = "RPG",
                IntegratedSecurity = true
            };

            return builder.ConnectionString;
        }

        public static void VerifyDatabase()
        {
            using(var conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                string sql =
                    "if not exists (select * from sys.tables where name='characters')" +
                    "   CREATE TABLE characters (" +
                    "       id int NOT NULL IDENTITY(1,1) PRIMARY KEY," +
                    "       name varchar(255) NOT NULL," +
                    "       hp int NOT NULL," +
                    "       strength int NOT NULL," +
                    "       agility int NOT NULL," +
                    "       intelligence int NOT NULL," +
                    "       stamina int NOT NULL," +
                    "       armor_rating int NOT NULL," +
                    "       thar0 int NOT NULL," +
                    "       class_type varchar(255) NOT NULL" +
                    "   )";

                using(var cmd = new SqlCommand(sql, conn)) cmd.ExecuteNonQuery();
            }
        }

        public static void SaveCharacter(Character character)
        {
            using(var conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                string sql = 
                    "INSERT INTO characters " +
                    "VALUES(" +
                    $"  '{character.Name.Replace("'", "''")}'," +
                    $"  {character.MaxHP}," +
                    $"  {character.Strength}," +
                    $"  {character.Agility}," +
                    $"  {character.Intelligence}," +
                    $"  {character.Stamina}," +
                    $"  {character.ArmorRating}," +
                    $"  {character.THAR0}," +
                    $"  '{character.GetType().ToString()}'" +
                    ")";

                using(var cmd = new SqlCommand(sql, conn)) cmd.ExecuteNonQuery();
            }
        }

        public static List<Character> LoadAllCharacters()
        {
            var result = new List<Character>();

            using(var conn = new SqlConnection(CONNECTION_STRING))
            using(var cmd = new SqlCommand("SELECT * FROM characters", conn))
            {
                conn.Open();

                using(var reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        string charJson = "{" +
                            $"\"$type\":\"{reader["class_type"]},CharacterLibrary\"," +
                            $"\"Intelligence\":\"{reader["intelligence"]}\"," +
                            $"\"Name\":\"{reader["name"]}\"," +
                            $"\"HP\":\"{reader["hp"]}\"," +
                            $"\"MaxHP\":\"{reader["hp"]}\"," +
                            $"\"Stamina\":\"{reader["stamina"]}\"," +
                            $"\"ArmorRating\":\"{reader["armor_rating"]}\"," +
                            $"\"Strength\":\"{reader["strength"]}\"," +
                            $"\"Agility\":\"{reader["agility"]}\"," +
                            $"\"THAR0\":\"{reader["thar0"]}\"" +
                        "}";

                        result.Add(FileHandler.FromJson(charJson));
                    }
                }
            }

            return result;
        }
    }
}
