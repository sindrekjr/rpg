﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

using CharacterLibrary;

namespace RPG
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SQL.VerifyDatabase();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(GetCharacters().ToList(), GetAllTypes().ToList()));
        }

        static IEnumerable<Character> GetCharacters()
        {
            return SQL.LoadAllCharacters();
            //return FileHandler.LoadAllCharacters();
        }

        static IEnumerable<Type> GetAllTypes() => typeof(Character).Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Character)));
    }
}
