# RPG

Winform RPG Character Generator. Uses an external library for the Character objects and its child instances. One can either use the accompanied CharacterLibrary.dll in lib/ or build the linked submodule CharacterLibrary/.

## Install
Open the solution in Visual Studio.

## Usage

```
Visual Studio Run
```

## Structure
The main project files are found under the folder ./RPG/. A function VerifyDatabase() is called at the [application entrypoint](https://gitlab.com/sindrekjr/rpg/-/blob/master/RPG/Program.cs) to verify that there is a database available, and will create necessary tables if they do not already exist.

## Related Documentation
* [Newtonsoft.Json](https://www.newtonsoft.com/json)

## License
None
